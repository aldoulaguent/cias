export class Cliente {
    id:number;
    nombre:string;
    primerapellido:string;
    segundoapellido:string;
    telefono:number;
    estatus:string;
    fechains:string;
    fechaup:string;
}
